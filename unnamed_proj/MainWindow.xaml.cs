﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

//John Stickney
//EECS 481
//HW2

namespace unnamed_proj
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
     
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void firebutton01_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxButton button = MessageBoxButton.YesNo;
            MessageBoxImage icon = MessageBoxImage.Question;

            MessageBoxResult result = MessageBox.Show("Confirm missle launch?", "Missile Launch Confirmation", button, icon);
            

            switch(result)
            {
                case MessageBoxResult.Yes:
                    MessageBoxResult result2 = MessageBox.Show("Nuclear launch detected!\nID: John Stickney\nClass: LGM-118A Peacekeeper (ICBM)", "Missile Detection System", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    switch (result2)
                    {
                        default:
                            this.Close();
                            break;
                    }
                    break;
                default:
                    break;
            }

            
        }
    }

}
